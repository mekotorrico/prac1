﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Text.Json;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace Cliente
{
    class Client
    {
        IPHostEntry host;
        IPAddress ipAddr;
        IPEndPoint endPoint;

        Socket s_Cliente;

        public Client(string ip, int port)
        {
            host = Dns.GetHostByName(ip);
            ipAddr = host.AddressList[0];
            endPoint = new IPEndPoint(ipAddr, port);

            s_Cliente = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            
            
        }
        public void Start()
        {
            s_Cliente.Connect(endPoint);
        }
         
        public string SendIP(string sms)
        {
            string smsEstado = "";
            byte[] byteSms=Encoding.ASCII.GetBytes(sms);
            s_Cliente.Send(byteSms);
            smsEstado = "Mensaje enviado";
            return smsEstado;
        }
        public string Recepcion()
        {
            byte[] respuesta = new byte[1024];
            s_Cliente.Receive(respuesta);
            return ab(respuesta);
        }

        public string ab(byte[]res)
        {
            string s;
            string info="";
            s = Encoding.ASCII.GetString(res);
           // info= JsonConvert.DeserializeObject
            var lista = JsonConvert.DeserializeAnonymousType(s, info);
            return lista;   //s
        }
    }
}
