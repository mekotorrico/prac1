﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cliente
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Client c;

        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void btnEnviar_Click(object sender, RoutedEventArgs e)
        {
            string estado = "";
            string sms; //= "funciona"
            sms = txtIp.Text;

            c = new Client("25.85.220.103", 4404);
            c.Start();

            estado = c.SendIP(sms);

            txtEstado.Text = (estado);

            //c = new Client("localhost", 4404);
            //c.Start();
            //while (true)
            //{
            //    sms = txtIp.Text;
            //   // sms = Microsoft.VisualBasic.Interaction.InputBox("Ingrese mensaje (IP)");
            //    estado = c.SendIP(sms);
            //    txtEstado.Text=(estado);
            //}
            if (txtEstado.Text != "")
            {
                txtInfo.Text = c.Recepcion();
            }

           
        }
    }
}
